//асинхронність - це коли код не виконується в по рядкам,як він прописаний, а є певні затримки у виконанні коду
const btn = document.querySelector("button");

async function getIP() {
    let response = await fetch('https://api.ipify.org/?format=json');
    let ip = await response.json();
    findIP(ip.ip);
}

async function findIP(request) {
    let response = await fetch(`http://ip-api.com/json/${request}?fields=continent,country,regionName,city,district`);
    let result = await response.json();
    document.body.insertAdjacentHTML("beforeend", `<p>continent:${result.continent}</p>
<p>country:${result.country}</p><p>regionName:${result.regionName}</p><p>city:${result.city}</p><p>district:${result.dictrict}</p>`)
    // console.log(result);
}

btn.addEventListener("click", getIP);










